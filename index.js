const express = require('express');
const sequelize = require('./src/database/db');
const apiRouter = require('./src/routes/api');
const app = express();
//require('./src/database/associations'); //**Si descomento esto, crashea **

app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use('/api', apiRouter);

app.listen(3000, function() {
    console.log('Listening in port 3000');
});

