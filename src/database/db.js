const { Sequelize } = require('sequelize');
const { database } = require('../config/config');
const addressBookModel = require('../models/addressBook');
const orderModel = require('../models/order');
const orderProductModel = require('../models/orderProduct');
const payMethodModel = require('../models/payMethod');
const productModel = require('../models/product');
const userModel = require('../models/user');
//const userAddressBookModel = require('../models/userAddressBook');

//require('../database/associations');

console.log(database.database);
console.log(database.username);
console.log(database.host);


const sequelize = new Sequelize(
    database.database,
    database.username,
    database.password, {
        host: database.host,
        dialect: 'mysql',
    });


const User = userModel(sequelize, Sequelize);
const AddressBook = addressBookModel(sequelize, Sequelize);
const Product = productModel(sequelize, Sequelize);
const PayMethod = payMethodModel(sequelize, Sequelize);
const Order = orderModel(sequelize, Sequelize);
const OrderProduct = orderProductModel(sequelize, Sequelize);

/** RELATIONS **/

/** USER - ADDRESS BOOK **/
User.hasMany(AddressBook, { foreignKey: 'user_id' });
AddressBook.belongsTo(User, { foreignKey: 'user_id' });

/** ORDER - PAY METHOD **/
PayMethod.hasOne(Order, { foreignKey: 'pay_id' });
Order.belongsTo(PayMethod, { foreignKey: 'pay_id' });

/** ORDER - PRODUCTS **/
//Order.belongsToMany(Product, { through: 'orderProduct', foreignKey: 'order_id' });
//Product.belongsToMany(Order, { through: 'orderProduct', foreignKey: 'product_id' });

/** USER - ORDER **/
User.hasOne(Order, { foreignKey: 'user_id' });
Order.belongsTo(User, { foreignKey: 'user_id' });

validar_conexion();

sequelize.sync({ force:false })
    .then(() => {
        console.log('Tablas sincronizadas');
    });

async function validar_conexion() {
    try {
        await sequelize.authenticate();
        console.log('Conexión establecida ok');
    } catch (error) {
        console.error('Error al conectarse a la bd: ', error);
    }
}



module.exports = {
    AddressBook,
    Order,
    OrderProduct,
    PayMethod,
    Product,
    User,
}