module.exports = (sequelize, DataTypes) => {

    const Order = sequelize.define('orders', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },        
    },
    {
        timestamps: false,
    });

    return Order;
    
};

//pay method id FK
        /*
        payMethod_id: {
            type: type.INTEGER,
            references: {
                model: 'payMethods',
                key: 'id',
            },
        },
        */