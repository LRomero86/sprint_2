module.exports = (sequelize, DataTypes) => {

    const OrderProduct = sequelize.define('orderProduct', {        
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        quantity: {
            type: DataTypes.INTEGER(99),
            allowNull: false,
        },
        
        //order id FK
        order_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'orders',
                key: 'id',
                allowNull: false,
            }
        },

        //product id FK
        product_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'products',
                key: 'id',
                allowNull: false,
            },
        },
        
    },
    {
        timestamps: false,
    });

    OrderProduct.associate = models => {
        OrderProduct.belongsTo(models.Order, { foreignKey: 'order_id' });
        OrderProduct.belongsTo(models.Product, { foreignKey: 'product_id' });
    }

    return OrderProduct;
};