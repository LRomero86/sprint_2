module.exports = (sequelize, DataTypes) => {
    
    const AddressBook = sequelize.define('AddressBook', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },        
        address: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        number: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        otherData: {
            type: DataTypes.STRING(40),
            allowNull: true,
        },
    },
    {
        timestamps: false,        
    });

    return AddressBook;
};