module.exports = (sequelize, DataTypes) => {

    const PayMethod = sequelize.define('payMethods', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        payType: {
            type: DataTypes.STRING(40),
            allowNull: false,
        },
    },
    {
        timestamps: false,
    });

    return PayMethod;
};