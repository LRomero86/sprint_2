module.exports = (sequelize, DataTypes) => {
   
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },        
        user: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        pass: {
            type: DataTypes.STRING(250),
            allowNull: false,
        },
        is_admin: {
            type: DataTypes.BOOLEAN,
            allowNull: false,            
        },
        is_logged: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        otherKey: {
            type: DataTypes.STRING(250),
            allowNull: true,
        },
    },
    {
        timestamps: false,
    });

    return User;
};