module.exports = (sequelize, DataTypes) => {
    
    const Product = sequelize.define('products', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        productName:{
            type: DataTypes.STRING,
            allowNull: false,
        },
        cost: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
    },
    {
        timestamps: false,
    });

    return Product;
};