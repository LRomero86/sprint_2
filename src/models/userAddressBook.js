module.exports = (sequelize, DataTypes) => {

    const UserAddress = sequelize.define('userAddressBooks', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        user_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'users',
                key: 'id',
                allowNull: false,
            },            
        },
        addresBook_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'addressBooks',
                key: 'id',
                allowNull: false,
            },
        },
    },
    {
        timestamps: false,
    });

    return UserAddress;
};