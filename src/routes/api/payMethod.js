const router = require('express').Router();
const { payMethods, PayMethod } = require('../../database/db');

//Listar Pay Method
router.get('/', async (req, res) => {
    const listPayMethod = await PayMethod.findAll();
    res.json(listPayMethod);
});

//Crear Pay Method
router.post('/', async (req, res) => {
    const newPayMethod = await PayMethod.create(req.body);
    res.status(200).json({ message: 'Medio de pago creado correctamente' });
});


module.exports = router;