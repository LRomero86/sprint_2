const router = require('express').Router();
const { orders, Order } = require('../../database/db');


//Listar Ordenes
router.get('/', async (req, res) => {
    const listOrder = await Order.findAll();    
    res.json(listOrder);
});

//Crear orden
router.post('/', async (req, res) => {
    const newOrder = await Order.create(req.body);
    res.status(200).json({ message: 'Orden creada correctamente' });
});


module.exports = router;