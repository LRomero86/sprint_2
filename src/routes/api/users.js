const router = require('express').Router();
const { users, User } = require('../../database/db');


//Listar usuarios
router.get('/', async (req, res) => {
    const listUsers = await User.findAll();
    res.json(listUsers);
});

//crear usuario
router.post('/', async (req, res) => {    
    const newUser = await User.create(req.body);
    res.status(200).json({ message: 'Usuario creado correctamente' });
});

//actualizar usuario
router.put('/:userId', async (req, res) => {
    const modUser = await User.update(req.body, {
        where:{ 
            id:req.params.userId,
        },
    });

    res.status(200).json({ message: 'Se ha modificado correctamente' });
});

//eliminar usuario
router.delete('/:userId', async (req, res) => {
    await User.destroy({
        where: {
            id:req.params.userId,
        },
    })
    .then(function(deleteUser) {
        if(deleteUser === 1) {
            res.status(200).json({message:"Usuario eliminado correctamente"});
        }
    })
    .catch(function(error) {
        res.status(201).json(error);
    });
});


module.exports = router;