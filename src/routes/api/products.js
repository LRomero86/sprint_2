const router = require('express').Router();
const { products, Product } = require('../../database/db');


//Listar Productos
router.get('/', async (req, res) => {
    const listProducts = await Product.findAll();    
    res.send(listProducts);
});

//Crear producto
router.post('/', async (req, res) => {
    const newProduct = await Product.create(req.body);
    res.status(200).json({ message: 'Producto creado correctamente' });
});

//Modificar producto
router.put('/:productId', async (req, res) => {
    const modProduct = await Product.update(req.body, {
        where:{
            id:req.params.productId,
        },
    });

    res.status(200).json({ message: 'Se ha modificado el producto correctamente' });
});

//Eliminar producto
router.delete('/:productId', async (req, res) => {
    await Product.destroy({
        where: {
            id:req.params.productId,
        },
    })
    .then(function(deleteProduct) {
        if(deleteProduct === 1) {
            res.status(202).json({ message: 'Producto eliminado correctamente' });
        }
    })
    .catch(function(error) {
        res.status(201).json(error);
    });
});


module.exports = router;