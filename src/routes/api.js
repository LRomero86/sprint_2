const router = require('express').Router();
const apiUsers = require('./api/users');
const apiProducts = require('./api/products')
const apiOrders = require('./api/orders');
const apiPayMethod = require('./api//payMethod');

router.use('/users', apiUsers);
router.use('/products', apiProducts);
router.use('/orders', apiOrders);
router.use('/pay_methods', apiPayMethod);

module.exports = router;